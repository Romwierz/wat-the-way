package pl.studenci.wattheway

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.os.Bundle
import android.widget.EditText
import androidx.activity.ComponentActivity
import androidx.activity.OnBackPressedCallback
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.animation.expandIn
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.Dash
import com.google.android.gms.maps.model.Gap
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.Marker
import com.google.maps.android.compose.MarkerState
import com.google.maps.android.compose.rememberCameraPositionState
import com.google.maps.android.compose.rememberMarkerState
import pl.studenci.wattheway.planWatu.FacilityMenager
import pl.studenci.wattheway.planWatu.Wydzial
import pl.studenci.wattheway.ui.theme.WatTheWayTheme
import com.google.maps.android.compose.Polyline
import com.google.maps.android.compose.rememberCameraPositionState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import pl.studenci.wattheway.ui.theme.WatTheWayTheme
import java.io.IOException
import java.io.InputStream
import java.util.Properties

import viewmodels.LocationViewModel
import util.ApiKeyUtil
import util.BitmapUtil

class MainActivity : ComponentActivity() {
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var apiKey: String
    private val locationViewModel: LocationViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        // Retrieve the API key
        apiKey = ApiKeyUtil().readApiKey(applicationContext)

        val requestPermissionLauncher = registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                getLastKnownLocation()
                startLocationUpdates()
            }
        }

        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        } else {
            getLastKnownLocation()
            startLocationUpdates()
        }

        setContent {
            WatTheWayTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    //Greeting("Android")
                    ShowMap(locationViewModel, apiKey)
                }
            }
        }

        onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (locationViewModel.destinationLocation != null) {
                    locationViewModel.clearRouteAndDestination()
                } else {
                    finish() // Exit the app if no destination is set
                }
            }
        })
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    val latLng = LatLng(it.latitude, it.longitude)
                    locationViewModel.setCurrentLocation(latLng)
                }
            }

        val locationRequest = com.google.android.gms.location.LocationRequest.create().apply {
            interval = 10000
            fastestInterval = 5000
            priority = com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            mainLooper
        )
    }

    private val locationCallback = object : com.google.android.gms.location.LocationCallback() {
        override fun onLocationResult(locationResult: com.google.android.gms.location.LocationResult) {
            locationResult.lastLocation?.let { location ->
                val latLng = LatLng(location.latitude, location.longitude)
                locationViewModel.setCurrentLocation(latLng)
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    val latLng = LatLng(it.latitude, it.longitude)
                    locationViewModel.setCurrentLocation(latLng)
                }
            }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ShowMap(locationViewModel: LocationViewModel, apiKey: String) {
    val wat = LatLng(52.25228, 20.90525)
    val cameraPositionState = rememberCameraPositionState {
        position = CameraPosition.fromLatLngZoom(wat, 15f)
    }

    // MutableState to keep track of markers
    var markers by remember { mutableStateOf(listOf<LatLng>()) }

    // Function to add a marker
    fun addMarker(marker: LatLng) {
        markers = markers + listOf(marker)
    }

    // Function to remove all markers
    fun clearMarkers() {
        markers = emptyList()
    }

    val currentLocation by rememberUpdatedState(locationViewModel.currentLocation)
    val destinationLocation by rememberUpdatedState(locationViewModel.destinationLocation)
    val routePoints by locationViewModel.routePoints

    Box(modifier = Modifier.fillMaxSize()) {
        GoogleMap(
            modifier = Modifier.fillMaxSize(),
            cameraPositionState = cameraPositionState,
            onMapClick = { clickedLatLng ->
                currentLocation?.let {
                    println("Map clicked at: $clickedLatLng")
                    locationViewModel.setDestinationLocation(clickedLatLng)
                    locationViewModel.fetchRoute(it, clickedLatLng, apiKey)
                } ?: run {
                    println("Current location is null")
                }
            }
        ) {
            markers.forEach { marker ->
                val markerState =
                    rememberMarkerState(position = LatLng(marker.latitude, marker.longitude))
                Marker(
                    //tytul pinezki nie moze zostac pobrany
                    //pinezki nie znikaja z mapy po ponownym wyszukaniu - zasmiecaja mape
                    title = "Budynek jakiś tam",
                    state = markerState,
                    snippet = "jebac kotlina i android studio",
                    draggable = false
                )
            }

            currentLocation?.let {
                Marker(
                    state = MarkerState(position = it),
                    icon = BitmapUtil().bitmapDescriptorFromVector(
                        LocalContext.current,
                        R.drawable.ic_current_location
                    ),
                    anchor = Offset(0.5f, 0.5f),
                    title = "Current Location"
                )
            }

            destinationLocation?.let {
                Marker(
                    state = MarkerState(position = it),
                    title = "Destination"
                )
            }

            if (routePoints.isNotEmpty()) {
                Polyline(
                    points = routePoints,
                    color = Color(0xFF0FB12E), // Light shade of green
                    width = 15f,
                    pattern = listOf(Dash(10f), Gap(10f)) // Specify the stroke pattern
                )
            }
        }

        // Call clearMarkers when the composable recomposes
        DisposableEffect(Unit) {
            markers = emptyList()
            clearMarkers()
            onDispose { }
        }

        var text by remember { mutableStateOf("") }
        var active by remember { mutableStateOf(false) }

        SearchBar(
            query = text,
            onQueryChange = { text = it },
            onSearch = {
                active = false

                val toSearch = FacilityMenager.searchBudynek(text)
                if (toSearch != null) {
                    //pobierane sa tylko wspolrzedne
                    val lat = toSearch.getLatitude()
                    val lon = toSearch.getLongitude()
                    val latLng = LatLng(lat, lon)
//                    addMarker(latLng)
                    currentLocation?.let {
                        println("Map clicked at: $latLng")
                        locationViewModel.setDestinationLocation(latLng)
                        locationViewModel.fetchRoute(it, latLng, apiKey)
                    } ?: run {
                        println("Current location is null")
                    }
                }
            },
            active = active,
            onActiveChange = {
                active = it
                text = ""
            },
            modifier = Modifier
                .fillMaxWidth(),
            placeholder = { Text(text = "Znajdź budynek") },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.Search,
                    contentDescription = "search"
                )
            },
        ) {}

        Button(
            onClick = {
                currentLocation?.let {
                    cameraPositionState.position = CameraPosition.fromLatLngZoom(it, 15f)
                }
            },
            colors = ButtonDefaults.buttonColors(
                containerColor = MaterialTheme.colorScheme.primary // Customize the color
            ),
            shape = RoundedCornerShape(27.dp), // Customize the button shape
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(16.dp) // Add padding to position the button away from the edges
                .size(65.dp) // Adjust size as needed
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_launcher_foreground),
                contentDescription = "Center Map",
                modifier = Modifier.size(100.dp), // Set the icon size
                colorFilter = androidx.compose.ui.graphics.ColorFilter.tint(MaterialTheme.colorScheme.onPrimary) // Tint the image
            )
        }
    }
}

