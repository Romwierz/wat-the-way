package pl.studenci.wattheway.planWatu

import java.util.function.Predicate

class FacilityMenager() {


    companion object{
        private val buildings = mutableListOf<Budynek>(

        Budynek("2137", Wydzial.BIBLIOTEKA,52.25345764013074, 20.89600852808771),
        Budynek("100", Wydzial.SZTAB, 52.25324960050845, 20.89969189516015),
        Budynek("45", Wydzial.WEL,52.25213322093549, 20.905225917095116),
        Budynek("47", Wydzial.WEL,52.25231659099523, 20.9056846347753),
        Budynek("42", Wydzial.WEL, 52.25231659099523, 20.9056846347753),
        Budynek("75", Wydzial.WEL,52.251251750276325, 20.905957359424402),
        Budynek("54A", Wydzial.WEL,52.25355993855498, 20.906121225392734),
        Budynek("61", Wydzial.WEL,52.25418895161166, 20.902414140667762),
        Budynek("29", Wydzial.WEL,52.24894735929488, 20.903629567727947),
        Budynek("80", Wydzial.WEL,52.24947905402087, 20.904713328193832),
        Budynek("114", Wydzial.WEL,52.249400179805846, 20.905296296580858),
        /*

        //koordynacje do dokończenia
        Budynek("68", Wydzial.WIM),
        Budynek("62", Wydzial.WIM),
        Budynek("54B", Wydzial.WIM),
        Budynek("19", Wydzial.WIM),
        Budynek("20", Wydzial.WIM),
        Budynek("21", Wydzial.WIM),
        Budynek("17", Wydzial.WIM),
        Budynek("23", Wydzial.WIM),
        Budynek("15", Wydzial.WIM),
        Budynek("28", Wydzial.WIM),
        Budynek("34A", Wydzial.WIM),

        Budynek("135", Wydzial.WLO),
        Budynek("22", Wydzial.WLO),
        Budynek("16", Wydzial.WLO),
        Budynek("158", Wydzial.WLO),



        Budynek("65", Wydzial.WCY),

        Budynek("98", Wydzial.WIG),
        Budynek("59", Wydzial.WIG),
        Budynek("58", Wydzial.WIG),
        Budynek("57", Wydzial.WIG),
        Budynek("53", Wydzial.WIG),
        Budynek("56", Wydzial.WIG),

        Budynek("63", Wydzial.WML),
        Budynek("66", Wydzial.WML),
        Budynek("67", Wydzial.WML),
        Budynek("74", Wydzial.WML),
        Budynek("72", Wydzial.WML),
        Budynek("69", Wydzial.WML),
        Budynek("36", Wydzial.WML),
        Budynek("25", Wydzial.WML),
        Budynek("26", Wydzial.WML),

        Budynek("55", Wydzial.WTC),
        Budynek("34", Wydzial.WTC),
        Budynek("5", Wydzial.WTC),
        Budynek("14", Wydzial.WTC),
        Budynek("24", Wydzial.WTC),

        Budynek("144", Wydzial.IOE),
        Budynek("41", Wydzial.IOE),
        Budynek("136", Wydzial.IOE),
        Budynek("46", Wydzial.IOE),
        Budynek("49", Wydzial.IOE),

        Budynek("13", Wydzial.default),
        Budynek("64", Wydzial.default),
        Budynek("35", Wydzial.default),
        Budynek("1", Wydzial.default),
        Budynek("10", Wydzial.default),
        Budynek("38", Wydzial.default),
        Budynek("43", Wydzial.default),
        Budynek("39", Wydzial.default),
        Budynek("131", Wydzial.default),
        Budynek("40", Wydzial.default),
        Budynek("60", Wydzial.default),
        Budynek("50", Wydzial.default),
        Budynek("52", Wydzial.default),
        Budynek("127", Wydzial.default),
        Budynek("125", Wydzial.default),
        Budynek("126", Wydzial.default),
        Budynek("121", Wydzial.default)
        */
        )

        fun filter(predicate: (Budynek) -> Boolean): List<Budynek>{

            return buildings.filter (predicate)

        }
        fun addBudynek(budynek: Budynek)
        {
            buildings += budynek
        }

        fun searchBudynek(numer: String): Budynek?
        {

            return buildings.find { it.numer == numer}

        }




    }

}