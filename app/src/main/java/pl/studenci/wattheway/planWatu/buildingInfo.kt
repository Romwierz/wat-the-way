package pl.studenci.wattheway.planWatu

import android.media.Image

data class buildingInfo(val automat: Automat, val iloscKanap:Int, val dystrybutorWody: Int)
{
   val machineImageList = mutableListOf<Image>()
    val relaxZoneImageList = mutableListOf<Image>()
    fun attachMachineImage(image :Image)
    {

        machineImageList.lastIndex.let {if (it< 0) 0 else it}
            .let{machineImageList.add(it,image)}
    }

    fun attachRelaxZoneImage(image: Image)
    {

        relaxZoneImageList.lastIndex.let {if (it < 0) 0 else it}
            .let { relaxZoneImageList.add(it, image) }

    }

}
