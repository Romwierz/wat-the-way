package pl.studenci.wattheway.planWatu

class Budynek(numer: String?, wydzial: Wydzial?, latitude:Double, longitude :Double) {

    val numer: String? = numer ?: "brak"
    val wydzial: Wydzial? = wydzial ?: Wydzial.default
    var list: List<Any> = mutableListOf(

        Automat.NONE

    )

    private var  latitude :Double = latitude
    private var longitude :Double = longitude

    fun addToList(elem: Any) {
        list += elem

    }

    fun editCoordinates(lat : Double, long: Double)
    {

        latitude = lat
        longitude = long
    }

    fun getLatitude() : Double
    {
        return latitude
    }

    fun getLongitude() : Double
    {
        return longitude
    }

}
