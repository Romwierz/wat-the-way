package pl.studenci.wattheway.planWatu

enum class Wydzial (val wydzial: String) {

    default("brak"),
    WLO("Wydział Bezpieczeństwa Logistyki i Zarządzania"),
    WCY("Wydział Cybernetyki"),
    WEL("Wydział Elektroniki"),
    WIG("Wydział Inżynierii Lądowej i Geodezji"),
    WIM("Wydział Inżynierii Mechanicznej"),
    WML("Wydział Mechatroniki, Uzbrojenia i Lotnictwa"),
    WTC("Wydział Nowych Technologii i Chemii"),
    IOE("Instytut Optoelektroniki"),
    SZTAB ("Sztab"),
    BIBLIOTEKA("Biblioteka");

}



enum class Automat(val opisAutomatu: String)
{
    NONE("brak automatu"),
    DRINK_MACHINE("Automat z piciem"),
    SNACK_MACHINE("Automat ze słodyczami"),
    DRINK_AND_SNACK_MACHINE("Automaty z piciem i słodyczami");
}