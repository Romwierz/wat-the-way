/*
Klasa umożliwiająca odczytania odpowiedniego klucza API
 */
package util

import android.content.Context
import java.io.IOException
import java.io.InputStream
import java.util.Properties

class ApiKeyUtil {
    fun readApiKey(context: Context): String {
        val properties = Properties()
        try {
            // Load the properties from secrets.properties
            val inputStream: InputStream = context.assets.open("secrets.properties")
            properties.load(inputStream)
            println("File read")
        } catch (e: IOException) {
            // Handle the exception if the file is not found or cannot be read
            println("File not read")
            e.printStackTrace()
        }
        // Return the value of the Directions_API_KEY property
        return properties.getProperty("Directions_API_KEY", "")
    }
}