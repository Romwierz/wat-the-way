package viewmodels

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject

class LocationViewModel : ViewModel() {
    private var _currentLocation by mutableStateOf<LatLng?>(null)
    val currentLocation: LatLng? get() = _currentLocation

    private var _destinationLocation by mutableStateOf<LatLng?>(null)
    val destinationLocation: LatLng? get() = _destinationLocation

    fun setCurrentLocation(location: LatLng) {
        _currentLocation = location
    }

    fun setDestinationLocation(location: LatLng) {
        _destinationLocation = location
    }

    fun clearRouteAndDestination() {
        _destinationLocation = null
        routePoints.value = emptyList()
    }

    val routePoints = mutableStateOf<List<LatLng>>(emptyList())

    fun fetchRoute(origin: LatLng, destination: LatLng, apiKey: String) {
        viewModelScope.launch {
            try {
                val client = OkHttpClient()
                val url = "https://maps.googleapis.com/maps/api/directions/json?" +
                        "origin=${origin.latitude},${origin.longitude}&" +
                        "destination=${destination.latitude},${destination.longitude}&" +
                        "mode=walking&" + // Specify the travel mode as walking
                        "key=$apiKey"

                println("Request URL: $url")  // Log the request URL

                val request = Request.Builder().url(url).build()

                val response = withContext(Dispatchers.IO) {
                    client.newCall(request).execute()
                }

                val data = response.body?.string()
                println("API Response: $data")  // Log the API response

                val json = JSONObject(data ?: "")
                val routes = json.getJSONArray("routes")
                if (routes.length() > 0) {
                    val overviewPolyline = routes.getJSONObject(0)
                        .getJSONObject("overview_polyline").getString("points")
                    val points = decodePolyline(overviewPolyline)
                    routePoints.value = points
                } else {
                    // Handle no routes found
                    println("No routes found")
                }
            } catch (e: Exception) {
                // Handle the exception
                println("Error fetching route: ${e.message}")
            }
        }
    }

    private fun decodePolyline(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].code - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].code - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng((lat / 1E5), (lng / 1E5))
            poly.add(p)
        }

        return poly
    }
}